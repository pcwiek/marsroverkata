sealed trait GeographicDirection
case object North extends GeographicDirection
case object South extends GeographicDirection
case object West extends GeographicDirection
case object East extends GeographicDirection

sealed trait TurnDirection
case object Left extends TurnDirection
case object Right extends TurnDirection

sealed trait MoveDirection
case object Forward extends MoveDirection
case object Backward extends MoveDirection
