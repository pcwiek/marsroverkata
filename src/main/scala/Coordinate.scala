case class Coordinate(x : Int, y : Int) {
  def move(delta : Coordinate) = Coordinate(x + delta.x, y + delta.y)
  def rollover(max : Size) = Coordinate(x % max.x, y % max.y)
}

case class Size(x : Int, y : Int)