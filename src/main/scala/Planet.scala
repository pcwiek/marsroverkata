class Planet(val size : Size, val obstaclesPlacement : IndexedSeq[Coordinate] = IndexedSeq()) {
  def isAtObstacle(location : Coordinate) = obstaclesPlacement.exists(c => c == location)
}
