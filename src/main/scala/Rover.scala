class Rover(var currentPosition : Coordinate, var currentDirection : GeographicDirection, private val planet : Planet){
  private val compass = IndexedSeq(North, East, South, West)
  private val offset = compass.size

  private def turn(direction : TurnDirection){
    currentDirection = compass((offset + compass.indexOf(currentDirection) + (direction match { case Left => -1 case Right => 1 })) % offset)
  }

  private def move(direction : MoveDirection) = {
    val stepDirection = direction match {
      case Forward => 1
      case Backward => -1
    }

    val delta = currentDirection match {
      case North => Coordinate(0, stepDirection)
      case South => Coordinate(0, -stepDirection)
      case West => Coordinate(-stepDirection, 0)
      case East => Coordinate(stepDirection, 0)
    }
    val nextCoordinate = currentPosition.move(delta).rollover(planet.size)
    if(planet.isAtObstacle(nextCoordinate)) None else Some(nextCoordinate)
  }

  def executeCommands(commands : Char*) = {
    import scala.annotation.tailrec
    @tailrec
    def execute(commands : List[Char], history : List[Char]) : List[Char] =
      commands match {
        case x :: xs =>
          val isSuccessful = x match {
            case 'F' => move(Forward) match {
              case Some(step) =>  currentPosition = step; true
              case None => false
            }
            case 'B' => move(Backward) match {
              case Some(step) => currentPosition = step; true
              case None => false
            }
            case 'L' => turn(Left); true
            case 'R' => turn(Right); true
          }

          if(isSuccessful)
            execute(xs, x :: history)
          else
            x :: history

        case Nil => history
      }
    val cmdList = commands.toList
    val history = execute(cmdList, List[Char]()).reverse
    if(history == cmdList)
      ("OK", history)
      // or
      // Right(history)
      // but the tests would have to be changed too.
    else
      ("NOT OK", history)
      // or
      // Left(history)
      // but the tests would have to be changed too.
  }
}