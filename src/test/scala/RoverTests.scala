import org.scalatest._

class RoverTests extends FlatSpec with Matchers {
  "Planet" can "define its size" in {
    val planet = new Planet(Size(7, 9))
    planet.size.x should be (7)
    planet.size.y should be (9)
  }

  "Rover" should "accept a starting coordinate and a direction its facing" in {
    val rover = new Rover(Coordinate(14, 22), East, new Planet(Size(40, 40)))
    rover.currentPosition.x should be (14)
    rover.currentPosition.y should be (22)
    rover.currentDirection should be (East)
  }

  it should "be able to move forward" in {
    val rover = new Rover(Coordinate(14, 22), East, new Planet(Size(40, 40)))
    rover.executeCommands('F')
    rover.currentPosition.x should be (15)
    rover.currentPosition.y should be (22)
  }

  it should "be able to move backward" in {
    val rover = new Rover(Coordinate(14, 22), East, new Planet(Size(40, 40)))
    rover.executeCommands('B')
    rover.currentPosition.x should be (13)
    rover.currentPosition.y should be (22)
  }

  it should "be able to turn left" in {
    val rover = new Rover(Coordinate(14, 22), East, new Planet(Size(40, 40)))
    rover.executeCommands('L')
    rover.currentDirection should be (North)
  }

  it should "be able to turn right" in {
    val rover = new Rover(Coordinate(14, 22), East, new Planet(Size(40, 40)))
    rover.executeCommands('R')
    rover.currentDirection should be (South)
  }

  it should "be able to execute a sequence of commands" in {
    val rover = new Rover(Coordinate(14, 22), North, new Planet(Size(40, 40)))
    rover.executeCommands('R', 'F', 'F', 'B', 'R')
    rover.currentPosition should be (Coordinate(15, 22))
    rover.currentDirection should be (South)
  }

  it should "wrap around the grid" in {
    val rover = new Rover(Coordinate(4,4), North, new Planet(Size(4, 4)))
    rover.executeCommands('F','R','F')
    rover.currentPosition.x should be (1)
    rover.currentPosition.y should be (1)
    rover.currentDirection should be (East)
  }

  it should "return OK and a full history if no obstacle was found" in {
    val rover = new Rover(Coordinate(4,4), North, new Planet(Size(20, 20)))
    val (result, history) = rover.executeCommands('F', 'F', 'L', 'F', 'R', 'B', 'B')
    rover.currentDirection should be (North)
    rover.currentPosition should be (Coordinate(3, 4))
    result should be ("OK")
    history should be (List('F', 'F', 'L', 'F', 'R', 'B', 'B'))
  }

  it should "return NOT OK and a history of commands up to the obstacle" in {
    val rover = new Rover(Coordinate(6,4), North, new Planet(Size(20, 20), IndexedSeq(Coordinate(7, 5))))
    val (result, history) = rover.executeCommands('F', 'F', 'R', 'F', 'L', 'B', 'R', 'F')
    rover.currentDirection should be (North)
    rover.currentPosition should be (Coordinate(7, 6))
    result should be ("NOT OK")
    history should be (List('F', 'F', 'R', 'F', 'L', 'B'))
  }
}
